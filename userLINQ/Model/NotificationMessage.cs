namespace userLINQ.Model;

/// <summary>
/// Representa un mensaje de notificación.
/// </summary>
public class NotificationMessage
{
    public string Action { get; set; }
    public string Details { get; set; }
}
