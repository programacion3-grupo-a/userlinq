namespace userLINQ.Model;

/// <summary>
/// Define la interfaz para un observador que recibe notificaciones.
/// </summary>
public interface IObserver
{
    /// <summary>
    /// Método llamado por el sujeto observable para notificar a este observador.
    /// </summary>
    /// <param name="message">El mensaje de notificación.</param>
    void Update(NotificationMessage message);
}
