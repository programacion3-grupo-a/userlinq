namespace userLINQ.Model;

/// <summary>
/// Representa una pila de errores.
/// </summary>
public class ExceptionStack
{
    private readonly Stack<ExceptionMessage> stack = new Stack<ExceptionMessage>();

    /// <summary>
    /// Agrega un nuevo mensaje de error a la pila.
    /// </summary>
    /// <param name="exceptionMessage">El mensaje de error a agregar.</param>
    public void PushError(string exceptionMessage)
    {
        stack.Push(new ExceptionMessage(exceptionMessage));
    }

    /// <summary>
    /// Elimina y devuelve el mensaje de error más reciente de la pila.
    /// </summary>
    /// <returns>El mensaje de error eliminado.</returns>
    public ExceptionMessage PopError()
    {
        return stack.Pop();
    }

    /// <summary>
    /// Obtiene todos los mensajes de error almacenados en la pila.
    /// </summary>
    /// <returns>Una enumeración de mensajes de error.</returns>
    public IEnumerable<ExceptionMessage> GetAllErrors()
    {
        return stack;
    }
}
