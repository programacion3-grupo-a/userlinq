namespace userLINQ.Model;

/// <summary>
/// Representa un sujeto observable que notifica a los observadores cuando ocurren ciertas acciones.
/// </summary>
public class NotificationSubject
{
    private List<IObserver> observers = new List<IObserver>();

    /// <summary>
    /// Agrega un observador a la lista de observadores.
    /// </summary>
    /// <param name="observer">El observador a agregar.</param>
    public void AddObserver(IObserver observer)
    {
        observers.Add(observer);
    }

    /// <summary>
    /// Elimina un observador de la lista de observadores.
    /// </summary>
    /// <param name="observer">El observador a eliminar.</param>
    public void RemoveObserver(IObserver observer)
    {
        observers.Remove(observer);
    }

    /// <summary>
    /// Notifica a todos los observadores registrados con un mensaje de notificación.
    /// </summary>
    /// <param name="message">El mensaje de notificación.</param>
    public void NotifyObservers(NotificationMessage message)
    {
        foreach (var observer in observers)
        {
            observer.Update(message);
        }
    }
}
