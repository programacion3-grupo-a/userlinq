using System.ComponentModel;
namespace userLINQ.Model;

/// <summary>
/// Representa un usuario con nombre, apellido y correo electrónico.
/// </summary>
public class Usuario : INotifyPropertyChanged
{
    private string nombre;
    private string apellido;
    private string correoElectronico;

    public string Nombre
    {
        get { return nombre; }
        set
        {
            if (nombre != value)
            {
                nombre = value;
                OnPropertyChanged(nameof(Nombre));
            }
        }
    }

    public string Apellido
    {
        get { return apellido; }
        set
        {
            if (apellido != value)
            {
                apellido = value;
                OnPropertyChanged(nameof(Apellido));
            }
        }
    }

    public string CorreoElectronico
    {
        get { return correoElectronico; }
        set
        {
            if (correoElectronico != value)
            {
                correoElectronico = value;
                OnPropertyChanged(nameof(CorreoElectronico));
            }
        }
    }

    public Usuario(string nombre, string apellido, string correo)
    {
        Nombre = nombre;
        Apellido = apellido;
        CorreoElectronico = correo;
    }

    /// <summary>
    /// Evento que se produce cuando una propiedad del objeto ha cambiado.
    /// </summary>
    public event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Invoca el evento PropertyChanged cuando cambia una propiedad.
    /// </summary>
    /// <param name="propertyName">El nombre de la propiedad que ha cambiado.</param>
    protected virtual void OnPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
