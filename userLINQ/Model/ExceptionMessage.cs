namespace userLINQ.Model;

/// <summary>
/// Representa un mensaje de error junto con una marca de tiempo.
/// </summary>
public class ExceptionMessage
{
    public string Message { get; set; }
    public DateTime TimeStamp { get; set; }

    public ExceptionMessage(string message)
    {
        Message = message;
        TimeStamp = DateTime.Now;
    }
}
