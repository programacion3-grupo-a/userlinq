# userLINQ

`userLINQ` es una aplicación de escritorio desarrollada en C# que permite la gestión de usuarios mediante la agregación de información personal, búsqueda, ordenación y notificación de acciones.

## Funcionalidades

### Agregar Usuarios
- Se proporcionan tres campos de entrada para el nombre, apellido y correo electrónico.
- Al hacer clic en un botón de envío, la información se guarda en una estructura de datos.
- La información se muestra en un ListView.

### Búsqueda y Ordenación
- Se agrega un campo adicional para buscar usuarios por correo electrónico,nombre y apellido.
- Se incluyen botones para ordenar la lista de usuarios de forma ascendente o descendente.
- Se utiliza LINQ para filtrar y buscar en la lista de usuarios.
- Se muestra un resumen en la parte inferior del ListView que indica el total de usuarios.

### Notificaciones
- Se implementa un sistema de notificación basado en el patrón observador.
- Cada vez que se realiza una acción en la aplicación, se muestra un mensaje de notificación.
- Las acciones incluyen agregar usuarios, ordenar y buscar.

### Manejo de Excepciones
- Se manejan posibles excepciones relacionadas con el procesamiento de usuarios.
- Los mensajes de error se guardan en una estructura de pila.
- Se proporciona un botón para mostrar los mensajes de error junto con la hora en que ocurrieron.

# Clases Principales

## MainViewModel
- Gestiona la lógica relacionada con la interfaz de usuario y la gestión de usuarios.
- Contiene propiedades para almacenar la información del usuario, la lista de usuarios mostrados y el mensaje de error.
- Implementa los comandos para agregar usuarios, buscar, ordenar y mostrar todos los usuarios.
- Utiliza el patrón observador para notificar acciones y manejar posibles excepciones.

## Usuario
- Representa un usuario con nombre, apellido y correo electrónico.
- Implementa la interfaz INotifyPropertyChanged para notificar cambios en las propiedades.
- Contiene propiedades para el nombre, apellido y correo electrónico del usuario.

## NotificationSubject y IObserver
- NotificationSubject representa un sujeto observable que notifica a los observadores cuando ocurren ciertas acciones.
- IObserver define la interfaz para un observador que recibe notificaciones.
- Se utilizan para implementar el sistema de notificación basado en el patrón observador.

## ExceptionStack y ExceptionMessage
- ExceptionStack representa una pila de excepciones que permite agregar, eliminar y obtener mensajes de excepción.
- ExceptionMessage representa un mensaje de excepción junto con una marca de tiempo.
a marca de tiempo. de error junto con una marca de tiempo
