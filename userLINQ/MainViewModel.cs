using System.Collections.ObjectModel;
using System.ComponentModel;
using userLINQ.Model;

namespace userLINQ;

/// <summary>
/// Clase que gestiona la lógica relacionada con la gestión de usuarios.
/// </summary>
public class MainViewModel : INotifyPropertyChanged, IObserver
{
    private NotificationSubject notificationSubject = new NotificationSubject();
    private readonly ExceptionStack errorStack = new ExceptionStack();

    private Usuario usuarioData;
    public Usuario UsuarioData
    {
        get { return usuarioData; }
        set
        {
            if (usuarioData != value)
            {
                usuarioData = value;
                OnPropertyChanged(nameof(UsuarioData));
            }
        }
    }

    private ObservableCollection<Usuario> usuariosOriginal;
    private ObservableCollection<Usuario> usuariosMostrados;
    public ObservableCollection<Usuario> UsuariosMostrados
    {
        get { return usuariosMostrados; }
        set
        {
            if (usuariosMostrados != value)
            {
                usuariosMostrados = value;
                OnPropertyChanged(nameof(UsuariosMostrados));
            }
        }
    }

    private int totalUsuarios;
    public int TotalUsuarios
    {
        get { return totalUsuarios; }
        set
        {
            if (totalUsuarios != value)
            {
                totalUsuarios = value;
                OnPropertyChanged(nameof(TotalUsuarios));
            }
        }
    }

    private string usuarioNoEncontradoMensaje;
    public string UsuarioNoEncontradoMensaje
    {
        get { return usuarioNoEncontradoMensaje; }
        set
        {
            if (usuarioNoEncontradoMensaje != value)
            {
                usuarioNoEncontradoMensaje = value;
                OnPropertyChanged(nameof(UsuarioNoEncontradoMensaje));
            }
        }
    }

    private string notificacionObservador;
    public string NotificacionObservador
    {
        get { return notificacionObservador; }
        set
        {
            if (notificacionObservador != value)
            {
                notificacionObservador = value;
                OnPropertyChanged(nameof(NotificacionObservador));
            }
        }
    }

    private string mensajeExeccion;
    public string MensajeExeccion
    {
        get { return mensajeExeccion; }
        set
        {
            if (mensajeExeccion != value)
            {
                mensajeExeccion = value;
                OnPropertyChanged(nameof(MensajeExeccion));
            }
        }
    }

    public ICommand AgregarUsuarioCommand { get; set; }
    public ICommand BuscarCommand { get; set; }
    public ICommand OrdenarAscendenteCommand { get; set; }
    public ICommand OrdenarDescendenteCommand { get; set; }
    public ICommand MostrarTodosUsuariosCommand { get; set; }

    public MainViewModel()
    {
        InitializeCommands();
        InitializeObservers();
        LoadInitialData();
    }

    /// <summary>
    /// Inicializa los comandos utilizados en la interfaz de usuario.
    /// </summary>
    private void InitializeCommands()
    {
        AgregarUsuarioCommand = new RelayCommand<object>(AgregarUsuario);
        BuscarCommand = new RelayCommand<string>(BuscarUsuario);
        OrdenarAscendenteCommand = new RelayCommand<object>(OrdenarAscendente);
        OrdenarDescendenteCommand = new RelayCommand<object>(OrdenarDescendente);
        MostrarTodosUsuariosCommand = new RelayCommand<object>(MostrarTodosUsuarios);
    }

    /// <summary>
    /// Inicializa los observadores para la notificación de cambios.
    /// </summary>
    private void InitializeObservers()
    {
        notificationSubject.AddObserver(this);
    }

    /// <summary>
    /// Carga los datos iniciales de los usuarios.
    /// </summary>
    private void LoadInitialData()
    {
        UsuarioData = new Usuario("", "", "");
        usuariosOriginal = new ObservableCollection<Usuario>
        {
            new Usuario("Juan", "Perez", "juan@example.com"),
            new Usuario("María", "González", "maria@example.com")
        };
        usuariosMostrados = new ObservableCollection<Usuario>(usuariosOriginal);
        TotalUsuarios = usuariosOriginal.Count;
    }

    /// <summary>
    /// Agrega un mensaje de error a la pila de errores.
    /// </summary>
    /// <param name="mensaje">Mensaje de error.</param>
    private void AgregarError(string mensaje)
    {
        errorStack.PushError(mensaje);
    }

    /// <summary>
    /// Muestra los mensajes de error.
    /// </summary>
    private void MostrarMensajesError()
    {
        foreach (var error in errorStack.GetAllErrors())
        {
            MensajeExeccion = ($"{error.TimeStamp}: {error.Message}");
        }
    }

    /// <summary>
    /// Actualiza la notificación del observador con el mensaje proporcionado.
    /// </summary>
    /// <param name="message">Mensaje de notificación.</param>
    public void Update(NotificationMessage message)
    {
        NotificacionObservador = $"Acción: {message.Action}. Detalles: {message.Details}";
    }

    /// <summary>
    /// Notifica una acción a los observadores.
    /// </summary>
    /// <param name="accion">Acción realizada.</param>
    /// <param name="detalles">Detalles de la acción.</param>
    private void NotificarAccion(string accion, string detalles)
    {
        var message = new NotificationMessage
        {
            Action = accion,
            Details = detalles
        };
        notificationSubject.NotifyObservers(message);
    }

    /// <summary>
    /// Muestra todos los usuarios en la lista mostrada.
    /// </summary>
    /// <param name="obj">Objeto de comando.</param>
    private void MostrarTodosUsuarios(object obj)
    {
        UsuariosMostrados = new ObservableCollection<Usuario>(usuariosOriginal);
        TotalUsuarios = UsuariosMostrados.Count;
        NotificarAccion("Mostrar todos los usuarios", "Se han mostrado todos los usuarios.");
    }

    /// <summary>
    /// Realiza una búsqueda de usuarios basada en el texto proporcionado.
    /// </summary>
    /// <param name="searchText">Texto de búsqueda.</param>
    public void BuscarUsuario(string searchText)
    {
        if (string.IsNullOrWhiteSpace(searchText))
        {
            UsuarioNoEncontradoMensaje = "Falta llenar el campo de búsqueda";
            return;
        }

        var usuariosFiltrados = new ObservableCollection<Usuario>(
            from usuario in usuariosOriginal
            where usuario.Nombre.Contains(searchText) || usuario.Apellido.Contains(searchText) || usuario.CorreoElectronico.Contains(searchText)
            select usuario);

        if (usuariosFiltrados.Count == 0)
        {
            UsuarioNoEncontradoMensaje = "Usuario no encontrado.";
        }
        else
        {
            UsuarioNoEncontradoMensaje = "";
            UsuariosMostrados = usuariosFiltrados;
        }

        TotalUsuarios = UsuariosMostrados.Count;
        NotificarAccion("Buscar usuarios", $"Búsqueda realizada: {searchText}");
    }

    /// <summary>
    /// Ordena los usuarios mostrados de manera ascendente basado en el correo electrónico.
    /// </summary>
    /// <param name="obj">Objeto de comando.</param>
    public void OrdenarAscendente(object obj)
    {
        var usuariosOrdenados = new ObservableCollection<Usuario>(
            from usuario in UsuariosMostrados
            orderby usuario.CorreoElectronico ascending
            select usuario);
        UsuariosMostrados = usuariosOrdenados;
        NotificarAccion("Ordenar ascendente", "Se han ordenado los usuarios de manera ascendente.");
    }

    /// <summary>
    /// Ordena los usuarios mostrados de manera descendente basado en el nombre.
    /// </summary>
    /// <param name="obj">Objeto de comando.</param>
    public void OrdenarDescendente(object obj)
    {
        var usuariosOrdenados = new ObservableCollection<Usuario>(
            from usuario in UsuariosMostrados
            orderby usuario.Nombre descending
            select usuario);
        UsuariosMostrados = usuariosOrdenados;
        NotificarAccion("Ordenar descendente", "Se han ordenado los usuarios de manera descendente.");
    }

    /// <summary>
    /// Agrega un nuevo usuario a la lista de usuarios.
    /// </summary>
    /// <param name="obj">Objeto de comando.</param>
    private void AgregarUsuario(object obj)
    {
        try
        {
            if (UsuarioData == null)
            {
                throw new Exception("El objeto UsuarioData es nulo.");
            }

            if (string.IsNullOrWhiteSpace(UsuarioData.Nombre) ||
                string.IsNullOrWhiteSpace(UsuarioData.Apellido) ||
                string.IsNullOrWhiteSpace(UsuarioData.CorreoElectronico))
            {
                throw new Exception("Falta llenar algún campo.");
            }

            usuariosOriginal.Add(UsuarioData);
            UsuarioData = new Usuario("", "", "");
            TotalUsuarios = usuariosOriginal.Count;
            MostrarTodosUsuarios(obj);
            NotificarAccion("Agregar usuario", "Se ha agregado un nuevo usuario.");
        }
        catch (Exception ex)
        {
            AgregarError($"Error al agregar usuario: {ex.Message}");
            MostrarMensajesError();
        }
    }

    public event PropertyChangedEventHandler PropertyChanged;
    protected void OnPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
